package Domain;

import java.util.ArrayList;

public class Order {
	
	private int id;
	private ClientDetails client;
	private Address deliveryAddress;
	private ArrayList<OrderItem> list;
	
	public Order(int id, ClientDetails client, Address deliveryAddress, OrderItem item) {
		this.id = id;
		this.client = client;
		this.deliveryAddress = deliveryAddress;
		this.list = new ArrayList<OrderItem>();
		list.add(item);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ClientDetails getClient() {
		return client;
	}

	public void setClient(ClientDetails client) {
		this.client = client;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public ArrayList<OrderItem> getList() {
		return list;
	}

	public void setItemIntoList(OrderItem item) {
		this.list.add(item);
	}
	
	
}
