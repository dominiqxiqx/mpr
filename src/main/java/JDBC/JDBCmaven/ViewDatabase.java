package JDBC.JDBCmaven;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class ViewDatabase {

	Connection connection;
	ResultSet resultSet;
	Statement statement;
	
	public ViewDatabase(Connection connection, Tables table) {
		
		try {
			this.connection = connection;
			statement = connection.createStatement();
			
			if (table.equals(Tables.ClientDetails)) {
				viewClientDetails();
			} else if (table.equals(Tables.Address)) {
				viewAddress();
			} else if (table.equals(Tables.OrderItem)) {
				viewOrderItem();
			} else if (table.equals(Tables.OrderSummary)) {
				viewOrderSummary();
			}
			
		} catch (Exception exc) {
			System.out.println(exc.getMessage() + ", zrodlo: konstruktor klasy VievDatabase");
		}
	}
	
	private void viewClientDetails() {
		
		String sql = "select * from clientdetails";
		
		try{
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				String surname = resultSet.getString("surname");
				String login = resultSet.getString("login");
				
				System.out.println("");
				System.out.println("Dane z tabeli ClientDetails:");
				System.out.println("id: " + id);
				System.out.println("name: " + name);
				System.out.println("surname: " + surname);
				System.out.println("login: " + login);
			}
			
		} catch (Exception exc) {
			System.out.println(exc.getMessage() + ", zrodlo: metoda viewClientDetails klasy VievDatabase");
		}
	}
	
private void viewAddress() {
		
		String sql = "select * from address";
		
		try{
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String street = resultSet.getString("street");
				String buildingNumber = resultSet.getString("buildingNumber");
				String flatNumber = resultSet.getString("flatNumber");
				String postalCode = resultSet.getString("postalCode");
				String city = resultSet.getString("city");
				String country = resultSet.getString("country");
				
				System.out.println("");
				System.out.println("Dane z tabeli Address:");
				System.out.println("id: " + id);
				System.out.println("street: " + street);
				System.out.println("buildingNumber: " + buildingNumber);
				System.out.println("flatNumber: " + flatNumber);
				System.out.println("postalCode: " + postalCode);
				System.out.println("city: " + city);
				System.out.println("country: " + country);
			}
			
		} catch (Exception exc) {
			System.out.println(exc.getMessage() + ", zrodlo: metoda viewAddress klasy VievDatabase");
		}
	}

private void viewOrderItem() {
	
	String sql = "select * from orderitem";
	
	try{
		resultSet = statement.executeQuery(sql);
		
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String description = resultSet.getString("description");
			double price = resultSet.getDouble("price");
			
			System.out.println("");
			System.out.println("Dane z tabeli OrderItem:");
			System.out.println("id: " + id);
			System.out.println("name: " + name);
			System.out.println("description: " + description);
			System.out.println("price: " + price);
		}
		
	} catch (Exception exc) {
		System.out.println(exc.getMessage() + ", zrodlo: metoda viewAddress klasy VievDatabase");
	}
}

private void viewOrderSummary() {
	
	String sql = "select * from ordersummary";
	
	try{
		resultSet = statement.executeQuery(sql);
		
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			int clientId = resultSet.getInt("clientId");
			int addressId = resultSet.getInt("addressId");
			//int orderItemId = resultSet.getInt("orderItemId");
		
			System.out.println("");
			System.out.println("Dane z tabeli OrderSummary:");
			System.out.println("id: " + id);
			if (clientId == 1) viewClientDetails();
			if (addressId == 1) viewAddress();
			viewOrderItem();
			break;
		}
		
	} catch (Exception exc) {
		System.out.println(exc.getMessage() + ", zrodlo: metoda viewAddress klasy VievDatabase");
	}
}
}
