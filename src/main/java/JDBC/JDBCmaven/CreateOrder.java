package JDBC.JDBCmaven;

import Domain.*;

public class CreateOrder {
		 
	private ClientDetails clientDetails;
	private Address address;
	private OrderItem orderItem1;
	private OrderItem orderItem2;
	private OrderItem orderItem3;
	private Order order;
	
	public CreateOrder () {
	
	clientDetails = new ClientDetails (1,
									"Adam",
									"Kowalski",
									"AdmKow");
	
	address = new Address(1,
						"Zwirownia",
						"23A", 
						"3", 
						"22-100", 
						"Chelm", 
						"Polska");
	
	orderItem1 = new OrderItem(1,
							"Laptop",
							"Bizesowy komputer firmy Toshiba",
							4000); 
	
	orderItem2 = new OrderItem(2,
							"Dysk NAS",
							"Sieciowy dysk HDD firmy Synology",
							700); 
	
	orderItem3 = new OrderItem(3,
							"Drukarka",
							"Drukarka laserowa firmy HP",
							1000);
	
	order = new Order(1,
					   clientDetails,
					    address,
					     orderItem1);
	
	order.setItemIntoList(orderItem2);
	order.setItemIntoList(orderItem3);
	}

	public ClientDetails getClientDetails() {
		return clientDetails;
	}

	public void setClientDetails(ClientDetails clientDetails) {
		this.clientDetails = clientDetails;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public OrderItem getOrderItem1() {
		return orderItem1;
	}

	public void setOrderItem1(OrderItem orderItem1) {
		this.orderItem1 = orderItem1;
	}

	public OrderItem getOrderItem2() {
		return orderItem2;
	}

	public void setOrderItem2(OrderItem orderItem2) {
		this.orderItem2 = orderItem2;
	}

	public OrderItem getOrderItem3() {
		return orderItem3;
	}

	public void setOrderItem3(OrderItem orderItem3) {
		this.orderItem3 = orderItem3;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	} 
	
	
	
}
