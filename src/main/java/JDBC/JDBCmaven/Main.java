package JDBC.JDBCmaven;

import java.sql.Connection;
import java.sql.DatabaseMetaData;

public class Main {

	public static void main(String[] args) {
		
		
		try{
		//ustanowienie polaczenia z baza danych HSQL
		Connection connection = (new Connector(Database.HSQLDB)).getConnection();
		
		//uzyskiwanie metadanych
		DatabaseMetaData md;
		md = connection.getMetaData();
		System.out.println(md.getDatabaseProductName());
		
		//tworzenie zamowienia
		CreateOrder createOrder = new CreateOrder();
		
		//wypelnianie tabel
		new CreateDatabaseStatement(connection, createOrder);
		
		//wyswietlenie poszczegolnych tabel
		new ViewDatabase (connection, Tables.ClientDetails);
		new ViewDatabase (connection, Tables.Address);
		new ViewDatabase (connection, Tables.OrderItem);
		new ViewDatabase (connection, Tables.OrderSummary);
		
		} catch (Exception exc) {
			System.out.println(exc.getMessage() + ", zrodlo: klasa Main");
		}
	}

}
