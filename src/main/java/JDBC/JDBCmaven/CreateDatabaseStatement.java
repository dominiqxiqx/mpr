package JDBC.JDBCmaven;

import java.sql.Connection;
import java.sql.Statement;

public class CreateDatabaseStatement {

	private Connection connection;
	private CreateOrder order;
	private Statement statement;
	
	
	private String createTableClient = "Create table ClientDetails("
									+ "id integer, name char(30), " 
									+ "surname char(30), login char(30))";
	
	private String createTableAddress = "Create table Address("
									+ "id integer, street char(50), " 
									+ "buildingNumber char(7), flatNumber char(5), "
									+ "postalCode char(10), city char(50), "
									+ "country char(30))";
	
	private String createTableOrderItem = "Create table OrderItem("
									+ "id integer, name char(50), " 
									+ "description char(150), price float)";
	
	private String createTableOrderSummary = "Create table OrderSummary("
										+ "id integer, clientId integer, " 
										+ "addressId integer, "
										+ "orderItemId integer)";
									
	public CreateDatabaseStatement (Connection connection, CreateOrder order) {
			this.connection = connection;
			this.order = order;
			createStatement();
			createTable();
			insertIntoAllTables();
		}
	
	private void createStatement() {
		try {
		this.statement = this.connection.createStatement(); 
		} catch(Exception exc) {
			System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: createStatement");
		}
	}
	
	private void createTable () {
		try {
		statement.executeUpdate(createTableClient);
		statement.executeUpdate(createTableAddress);
		statement.executeUpdate(createTableOrderItem);
		statement.executeUpdate(createTableOrderSummary);
		System.out.println("utworzono tabele!");
		
		} catch (Exception exc) {
			System.out.println(exc.getMessage());
		}
	}
	
	private void insertIntoAllTables () {
		insertIntoTableClient();
		insertIntoTableAddress();
		insertIntoTableOrderItem_1();
		insertIntoTableOrderItem_2();
		insertIntoTableOrderItem_3();
		insertIntoTableOrderSummary();
	}
	
	private void insertIntoTableClient () {
		
		 int id = order.getClientDetails().getId();
		 String name = order.getClientDetails().getName();
		 String surname = order.getClientDetails().getSurname();
		 String login = order.getClientDetails().getLogin();
		 
		 String sql = "insert into ClientDetails (id, name, surname, login) values (" 
				 		+id+ ", '" +name+ "', '" +surname+ "', '" +login+ "')";
		 
		 try {
		 statement.executeUpdate(sql);
		 System.out.println("Uzupelniono tabele ClientDetails");
		 } catch (Exception exc) {
				System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: insertIntotableClient");

		 }
	}
	
	private void insertIntoTableAddress () {
		
		 int id = order.getAddress().getId();
		 String street = order.getAddress().getStreet();
		 String buildingNumber = order.getAddress().getBuildingNumber();
		 String flatNumber = order.getAddress().getFlatNumber();
		 String postalCode = order.getAddress().getPostalCode();
		 String city = order.getAddress().getCity();
		 String country = order.getAddress().getCountry();
		 
		 String sql = "insert into Address (id, street, buildingNumber, flatNumber, postalCode, city, country) values (" 
				 		+id+ ", '" +street+ "', '" +buildingNumber+ "', '" +flatNumber+ "', '"
		 				+postalCode+ "', '" +city+ "', '" +country+ "')";
		 try {
		 statement.executeUpdate(sql);
		 System.out.println("Uzupelniono tabele Address");
		 } catch (Exception exc) {
				System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: insertIntoTableAddress");

		 }
	}
	
	private void insertIntoTableOrderItem_1 () {
		
		 int id = order.getOrderItem1().getId();
		 String name = order.getOrderItem1().getName();
		 String description = order.getOrderItem1().getDescription();
		 double price = order.getOrderItem1().getPrice();
		 		 
		 String sql = "insert into OrderItem (id, name, description, price) values (" 
				 		+id+ ", '" +name+ "', '" +description+ "', " +price+ ")";
		 try {
		 statement.executeUpdate(sql);
		 System.out.println("Uzupelniono tabele OrderItem");
		 } catch (Exception exc) {
				System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: insertIntoOrderItem_1");

		 }
	}
	
	private void insertIntoTableOrderItem_2 () {
		
		 int id = order.getOrderItem2().getId();
		 String name = order.getOrderItem2().getName();
		 String description = order.getOrderItem2().getDescription();
		 double price = order.getOrderItem2().getPrice();
		 		 
		 String sql = "insert into OrderItem (id, name, description, price) values (" 
				 		+id+ ", '" +name+ "', '" +description+ "', " +price+ ")";
		 try {
		 statement.executeUpdate(sql);
		 System.out.println("Uzupelniono tabele OrderItem");
		 } catch (Exception exc) {
				System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: insertIntoOrderItem_2");

		 }
	}
	
	private void insertIntoTableOrderItem_3 () {
		
		 int id = order.getOrderItem3().getId();
		 String name = order.getOrderItem3().getName();
		 String description = order.getOrderItem3().getDescription();
		 double price = order.getOrderItem3().getPrice();
		 		 
		 String sql = "insert into OrderItem (id, name, description, price) values (" 
				 		+id+ ", '" +name+ "', '" +description+ "', " +price+ ")";
		 try {
		 statement.executeUpdate(sql);
		 System.out.println("Uzupelniono tabele OrderItem");
		 } catch (Exception exc) {
				System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: insertIntoOrderItem_3");

		 }
	}
	
	private void insertIntoTableOrderSummary () {
		
		 int id = order.getOrder().getId();
		 int clientId = order.getOrder().getClient().getId();	
		 int addressId = order.getOrder().getDeliveryAddress().getId();
		 int orderItemId_1 = order.getOrder().getList().get(0).getId();
		 int orderItemId_2 = order.getOrder().getList().get(1).getId();
		 int orderItemId_3 = order.getOrder().getList().get(2).getId();
		 		 
		 String sql1 = "insert into OrderSummary (id, clientId, addressId, orderItemId) values (" 
				 		+id+ ", " +clientId+ ", " +addressId+ ", " +orderItemId_1+ ")";
		 
		 String sql2 = "insert into OrderSummary (id, clientId, addressId, orderItemId) values (" 
			 		+id+ ", " +clientId+ ", " +addressId+ ", " +orderItemId_2+ ")";
		 
		 String sql3 = "insert into OrderSummary (id, clientId, addressId, orderItemId) values (" 
			 		+id+ ", " +clientId+ ", " +addressId+ ", " +orderItemId_3+ ")";
		 
		 try {
		 statement.executeUpdate(sql1);
		 statement.executeUpdate(sql2);
		 statement.executeUpdate(sql3);
		 System.out.println("Uzupelniono tabele OrderSummary");
		 } catch (Exception exc) {
				System.out.println(exc.getMessage() + ", Klasa: CreateDatabaseStatement, metoda: insertIntoTableOrderSummary");

		 }
	}
}
